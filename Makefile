PACKAGES = data packages/*

down:
	docker-compose down
.PHONY: down

install:
	npm ci

	for dir in ${PACKAGES}; do \
  		if [ -d $${dir} ]; then \
			(cd $${dir} && npm ci); \
		fi; \
  	done
.PHONY: install

run:
	docker-compose run \
		--rm \
		--service-ports \
		${SERVICE} \
		${CMD}
.PHONY: run

serve:
	docker-compose up
.PHONY: serve

uninstall:
	rm -Rf node_modules
	rm -Rf .git/hooks

	for dir in ${PACKAGES}; do \
		if [ -d $${dir} ]; then \
			(cd $${dir} && rm -Rf node_modules); \
		fi; \
  	done
.PHONY: uninstall
