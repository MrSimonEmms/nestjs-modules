/**
 * session.entity
 */

/* Node modules */

/* Third-party modules */
import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { IsDate, IsNotEmpty, IsOptional, IsUUID } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';

/* Files */
import { IUser } from '../interfaces/user.interface';
// eslint-disable-next-line import/no-cycle
import User from './user.entity';

@Entity('session')
@Index(['expires'])
@Index(['token', 'expires'])
export default class SessionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @IsOptional({ groups: [CrudValidationGroups.UPDATE] })
  @IsNotEmpty({ always: true })
  @IsUUID(4, { always: true })
  @Column({
    type: 'varchar',
    length: 200,
    unique: true,
  })
  token: string;

  @IsOptional({ groups: [CrudValidationGroups.UPDATE] })
  @IsNotEmpty({ always: true })
  @IsDate({ always: true })
  @Column({
    type: 'datetime',
  })
  expires: Date;

  @ManyToOne(() => User, (user) => user.id, {
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  user: IUser;

  @CreateDateColumn({
    type: 'datetime',
  })
  createdAt: Date;
}
