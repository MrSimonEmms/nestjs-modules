/**
 * user.entity
 */

/* Node modules */
import { BinaryLike, pbkdf2, randomBytes } from 'crypto';

/* Third-party modules */
import { Injectable } from '@nestjs/common';
import {
  AfterLoad,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { IsEmail, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { CrudValidationGroups } from '@nestjsx/crud';
import { Exclude } from 'class-transformer';

/* Files */
import { IUser, ISession } from '../interfaces/user.interface';
import IsUnique from '../validation/isUnique';
// eslint-disable-next-line import/no-cycle
import Session from './session.entity';

const passwordJoin = '$';
const entityName = 'user';

@Injectable()
@Entity(entityName)
export default class UserEntity implements IUser {
  @Exclude()
  private tempPassword: string;

  @PrimaryGeneratedColumn()
  id: number;

  @IsOptional({ groups: [CrudValidationGroups.UPDATE] })
  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  @MaxLength(200, { always: true })
  @Column({
    type: 'varchar',
    length: 200,
  })
  name: string;

  @IsOptional({ groups: [CrudValidationGroups.UPDATE] })
  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  @MaxLength(200, { always: true })
  @IsEmail({}, { always: true })
  @IsUnique('id', { always: true })
  @Column({
    type: 'varchar',
    length: 200,
    unique: true,
  })
  emailAddress: string;

  @IsOptional({ groups: [CrudValidationGroups.UPDATE] })
  @IsNotEmpty({ always: true })
  @IsString({ always: true })
  @MinLength(6, { always: true })
  @MaxLength(50, { always: true })
  @Column({
    type: 'varchar',
    length: 200,
  })
  password: string;

  @OneToMany(() => Session, (session) => session.user)
  sessions: ISession[];

  @CreateDateColumn({
    type: 'datetime',
    update: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'datetime',
    update: false,
  })
  updatedAt: Date;

  @AfterLoad()
  private loadTempPassword(): void {
    this.tempPassword = this.password;
  }

  @BeforeInsert()
  async beforeInsert() {
    await this.encryptPassword();
  }

  @BeforeUpdate()
  async beforeUpdate() {
    await this.encryptPassword();
  }

  /**
   * Encrypt Password
   *
   * Encrypt the password if it's deemed necessary
   *
   * @return Promise<void>
   */
  private async encryptPassword(): Promise<void> {
    if (this.password !== this.tempPassword) {
      const salt = randomBytes(16).toString('hex');
      const hash = await UserEntity.hashPassword(this.password, salt);
      this.password = [salt, hash].join(passwordJoin);
      this.tempPassword = this.password;
    }
  }

  /**
   * Verify Password
   *
   * Compares the input password with the stored hash
   *
   * @param password {string}
   * @return Promise<boolean>
   */
  async verifyPassword(password: string): Promise<boolean> {
    const [salt, originalHash] = this.password.split('$');

    return (await UserEntity.hashPassword(password, salt)) === originalHash;
  }

  /**
   * Hash Password
   *
   * Method to generate the password hash. Favoured the PBKDF2 algorithm
   * over BCrypt as this is natively support - BCrypt requires a C/C++
   * addon which adds complexity to deployments.
   *
   * @param {BinaryLike} password
   * @param {BinaryLike} salt
   * @returns {Promise<string>}
   * @private
   */
  private static async hashPassword(password: BinaryLike, salt: BinaryLike): Promise<string> {
    const iterations = 2048;
    const keyLength = 32;
    const digest = 'sha512';

    return new Promise<string>((resolve, reject) => {
      pbkdf2(password, salt, iterations, keyLength, digest, (err, result) => {
        if (err) {
          reject(err);
          return;
        }

        resolve(result.toString('hex'));
      });
    });
  }
}
