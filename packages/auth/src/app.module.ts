/**
 * app.module
 */

/* Node modules */
import * as path from 'path';

/* Third-party modules */
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { LoggerModule, Params, PinoLogger } from 'nestjs-pino';
import { Logger as TypeOrmLogger } from 'typeorm';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { LoggerOptions } from 'typeorm/logger/LoggerOptions';
import { TerminusModule } from '@nestjs/terminus';

/* Files */
import config from './config';
import PinoTypeOrmLogger from './lib/logger';
import HealthController from './controllers/health.controller';
import SessionEntity from './entities/session.entity';
import SessionStrategy from './auth/strategies/session.strategy';
import UserController from './controllers/user.controller';
import UserEntity from './entities/user.entity';
import UserService from './services/user.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: config,
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService): Promise<Params> =>
        configService.get<Params>('logger'),
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule, LoggerModule],
      inject: [ConfigService, PinoLogger],
      useFactory: async (
        configService: ConfigService,
        pinoLogger: PinoLogger,
      ): Promise<TypeOrmModuleOptions> => {
        let logger: 'debug' | TypeOrmLogger = 'debug';
        let logging: boolean = false;

        if (configService.get<LoggerOptions>('db.logging', false)) {
          logger = new PinoTypeOrmLogger(pinoLogger);
          logging = true;
        }

        return {
          /* Types are given where we need to override type system */
          logger,
          logging,
          type: configService.get<'mysql' | 'mariadb'>('db.type', 'mysql'),
          host: configService.get<string>('db.host'),
          port: configService.get<number>('db.port'),
          username: configService.get<string>('db.username'),
          password: configService.get<string>('db.password'),
          database: configService.get<any>('db.database'),
          migrationsRun: configService.get<boolean>('db.migrationsRun', true),
          synchronize: configService.get<boolean>('db.synchronize', false),
          ssl: configService.get<any>('db.ssl'),
          entities: [path.join(__dirname, '**', '*.entity{.ts,.js}')],
          migrations: [path.join(__dirname, 'migrations', '*{.ts,.js}')],
        };
      },
    }),
    TerminusModule,
    TypeOrmModule.forFeature([SessionEntity, UserEntity]),
  ],
  controllers: [HealthController, UserController],
  providers: [SessionStrategy, UserService],
})
export default class AppModule {}
