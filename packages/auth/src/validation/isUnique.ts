/**
 * isUnique
 */

/* Node modules */

/* Third-party modules */
import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';
import { getManager, Not } from 'typeorm';

/* Files */

export default function isUnique(recordIdColumn: string, validationOptions?: ValidationOptions) {
  return (object: Object, propertyName: string) => {
    registerDecorator({
      propertyName,
      async: true,
      constraints: [recordIdColumn],
      options: validationOptions,
      target: object.constructor,
      validator: {
        defaultMessage(): string {
          return '$property must be unique';
        },

        async validate(value: string, args: ValidationArguments): Promise<boolean> {
          const recordId = args.object[recordIdColumn];

          const existingRecord = await getManager().findOne(args.targetName, {
            [args.property]: value,
            [recordIdColumn]: Not(recordId),
          });

          return !existingRecord;
        },
      },
    });
  };
}
