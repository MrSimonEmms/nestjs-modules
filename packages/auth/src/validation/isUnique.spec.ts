/**
 * isUnique.spec
 */

/* eslint-disable import/first */

/* Mocks */
jest.mock('class-validator');
jest.mock('typeorm');

/* Node modules */

/* Third-party modules */
import { registerDecorator } from 'class-validator';
import { getManager, Not } from 'typeorm';

/* Files */
import isUnique from './isUnique';

describe('isUnique', () => {
  let defaultMessage;
  let validate;
  let recordIdColumn;

  beforeEach(() => {
    recordIdColumn = 'recordId';
    const validationOptions: any = 'validationOpts';
    const object: any = 'object';
    const propertyName = 'propertyName';

    isUnique(recordIdColumn, validationOptions)(object, propertyName);

    expect(registerDecorator).toBeCalledWith(
      expect.objectContaining({
        propertyName,
        async: true,
        constraints: [recordIdColumn],
        options: validationOptions,
        target: object.constructor,
      }),
    );

    const [[call]] = (<any>registerDecorator).mock.calls;

    defaultMessage = call.validator.defaultMessage;
    validate = call.validator.validate;
  });

  describe('#defaultMessage', () => {
    it('should return the default message', () => {
      expect(defaultMessage()).toBe('$property must be unique');
    });
  });

  describe('#validate', () => {
    it('should call the database and return the opposite status', async () => {
      const existing = true;
      const findOne = jest.fn().mockResolvedValue(existing);
      (<any>getManager).mockReturnValue({
        findOne,
      });

      const value = 'value';
      const args = {
        object: {
          [recordIdColumn]: 'some-column',
        },
        targetName: 'some-target',
        property: 'some-property',
      };

      expect(await validate(value, args)).toEqual(!existing);

      expect(getManager).toBeCalledWith();
      expect(findOne).toBeCalledWith(args.targetName, {
        [args.property]: value,
        [recordIdColumn]: Not(args.object[recordIdColumn]),
      });
    });
  });
});
