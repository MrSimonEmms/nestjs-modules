/**
 * user.service
 */

/* Node modules */

/* Third-party modules */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ConfigService } from '@nestjs/config';
import { MoreThanOrEqual } from 'typeorm';
import * as ms from 'ms';
import * as uuid from 'uuid';

/* Files */
import UserEntity from '../entities/user.entity';
import { IUserDTO } from '../interfaces/user.interface';
import SessionEntity from '../entities/session.entity';

@Injectable()
export default class UserService extends TypeOrmCrudService<UserEntity> {
  constructor(
    @InjectRepository(UserEntity) repo,
    @InjectRepository(SessionEntity) private session,
    private configService: ConfigService,
  ) {
    super(repo);
  }

  async findByEmailAndPassword(emailAddress: string, password: string): Promise<UserEntity | void> {
    const user = await this.repo.findOne({
      emailAddress,
    });

    if (!user) {
      return undefined;
    }

    if (!(await user.verifyPassword(password))) {
      return undefined;
    }

    return user;
  }

  async findBySessionToken(token: string): Promise<UserEntity | void> {
    const session = await this.session.findOne({
      where: {
        token,
        expires: MoreThanOrEqual(new Date()),
      },
      relations: ['user'],
    });

    if (!session) {
      return undefined;
    }

    const { user } = session;

    return user;
  }

  async generateUserToken(user: UserEntity): Promise<{ expires: Date; token: string }> {
    /* Get the expiry time for public consumption */
    const expiresIn = this.configService.get<string>('session.expiry');
    const expires = new Date(Date.now() + ms(expiresIn));
    const token = uuid.v4();

    await this.session.insert({
      token,
      expires,
      user,
      createdAt: new Date(),
    });

    return {
      expires,
      token,
    };
  }

  toDTO(user: UserEntity): IUserDTO {
    const dto = {
      ...user,
    };

    delete dto.password;
    delete (<any>dto).tempPassword;

    return dto;
  }
}
