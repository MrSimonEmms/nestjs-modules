/**
 * user.service.spec
 */

/* eslint-disable import/first */

/* Mocks */
jest.mock('uuid');

/* Node modules */

/* Third-party modules */
import { Test, TestingModule } from '@nestjs/testing';
import { ConfigService } from '@nestjs/config';
import { getRepositoryToken } from '@nestjs/typeorm';
import { MoreThanOrEqual } from 'typeorm';
import * as ms from 'ms';
import * as uuid from 'uuid';

/* Files */
import UserService from './user.service';
import UserEntity from '../entities/user.entity';
import MockRepository from '../../test/mockRepository';
import SessionEntity from '../entities/session.entity';

describe('UserService', () => {
  let userService: UserService;
  let userEntity: MockRepository;
  let sessionEntity: MockRepository;
  let configService: ConfigService;
  let timers;

  beforeEach(async () => {
    timers = jest.useFakeTimers('modern');
    timers.setSystemTime(new Date('2020-01-01').getTime());

    const app: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(UserEntity),
          useValue: new MockRepository(),
        },
        {
          provide: getRepositoryToken(SessionEntity),
          useValue: new MockRepository(),
        },
        {
          provide: ConfigService,
          useFactory: () => ({
            get: jest.fn(),
          }),
        },
      ],
    }).compile();

    userEntity = app.get(getRepositoryToken(UserEntity));
    sessionEntity = app.get(getRepositoryToken(SessionEntity));
    configService = app.get(ConfigService);
    userService = app.get(UserService);
  });

  afterEach(() => {
    timers.clearAllTimers();
  });

  describe('#findByEmailAndPassword', () => {
    it('should return undefined if no user found', async () => {
      const emailAddress = 'someemail';
      const password = 'somepass';

      userEntity.findOne.mockResolvedValue(undefined);

      expect(await userService.findByEmailAndPassword(emailAddress, password)).toBeUndefined();

      expect(userEntity.findOne).toBeCalledWith({
        emailAddress,
      });
    });

    it('should return undefined if verifyPassword method is false', async () => {
      const user = {
        verifyPassword: jest.fn().mockReturnValue(false),
      };
      const emailAddress = 'validEmail';
      const password = 'invalidpass';

      userEntity.findOne.mockResolvedValue(user);

      expect(await userService.findByEmailAndPassword(emailAddress, password)).toBeUndefined();

      expect(userEntity.findOne).toBeCalledWith({
        emailAddress,
      });

      expect(user.verifyPassword).toBeCalledWith(password);
    });

    it('should return the user if verifyPassword method is true', async () => {
      const user = {
        verifyPassword: jest.fn().mockReturnValue(true),
      };
      const emailAddress = 'validEmail';
      const password = 'validpass';

      userEntity.findOne.mockResolvedValue(user);

      expect(await userService.findByEmailAndPassword(emailAddress, password)).toBe(user);

      expect(userEntity.findOne).toBeCalledWith({
        emailAddress,
      });

      expect(user.verifyPassword).toBeCalledWith(password);
    });
  });

  describe('#findBySessionToken', () => {
    it('should return undefined if no session found', async () => {
      const token = 'some-bad-token';
      const date = new Date();

      sessionEntity.findOne.mockResolvedValue(undefined);

      expect(await userService.findBySessionToken(token)).toBe(undefined);

      expect(sessionEntity.findOne).toBeCalledWith({
        where: {
          token,
          expires: MoreThanOrEqual(date),
        },
        relations: ['user'],
      });
    });

    it('should return the user if session found', async () => {
      const token = 'some-token';
      const session = {
        user: 'some-user',
      };

      sessionEntity.findOne.mockResolvedValue(session);

      expect(await userService.findBySessionToken(token)).toBe(session.user);

      expect(sessionEntity.findOne).toBeCalledWith({
        where: {
          token,
          expires: MoreThanOrEqual(new Date()),
        },
        relations: ['user'],
      });
    });
  });

  describe('#generateUserToken', () => {
    it('should generate a token and insert into table', async () => {
      const timeout = '2d';
      const token = 'some-token';
      const user: any = 'user-detail';
      const now = Date.now();

      (configService.get as any).mockReturnValue(timeout);
      (uuid.v4 as any).mockReturnValue(token);

      expect(await userService.generateUserToken(user)).toEqual({
        token,
        expires: new Date(now + ms(timeout)),
      });

      expect(sessionEntity.insert).toBeCalledWith({
        token,
        user,
        expires: new Date(now + ms(timeout)),
        createdAt: new Date(),
      });
    });
  });

  describe('#toDTO', () => {
    it('should return the user object without the password and tmpPassword keys', () => {
      const user: any = {
        id: 1,
        name: 'some-name',
        emailAddress: 'some@email.com',
        password: 'some-password',
        tempPassword: 'some-tmppass',
      };

      expect(userService.toDTO(user)).toEqual({
        ...user,
        password: undefined,
        tempPassword: undefined,
      });
    });
  });
});
