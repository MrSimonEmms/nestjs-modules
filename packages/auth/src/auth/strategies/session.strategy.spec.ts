/**
 * session.strategy.spec
 */

/* Node modules */

/* Third-party modules */
import { Test, TestingModule } from '@nestjs/testing';
import { UnauthorizedException } from '@nestjs/common';

/* Files */
import SessionStrategy from './session.strategy';
import UserService from '../../services/user.service';

describe('SessionStrategy', () => {
  let sessionStrategy: SessionStrategy;
  let userService: any;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [
        SessionStrategy,
        {
          provide: UserService,
          useFactory: () => ({
            findBySessionToken: jest.fn(),
          }),
        },
      ],
    }).compile();

    userService = app.get(UserService);
    sessionStrategy = app.get<SessionStrategy>(SessionStrategy);
  });

  describe('#validate', () => {
    it('should throw unauthorized exception if user not found', async () => {
      const token = 'some-bad-token';
      userService.findBySessionToken.mockResolvedValue();

      try {
        await sessionStrategy.validate(token);

        expect('invalid').toBe(true);
      } catch (err) {
        expect(err).toBeInstanceOf(UnauthorizedException);
      }

      expect(userService.findBySessionToken).toBeCalledWith(token);
    });

    it('should user if found', async () => {
      const user = 1;
      const token = 'some-token';
      userService.findBySessionToken.mockResolvedValue(user);

      expect(await sessionStrategy.validate(token)).toBe(user);

      expect(userService.findBySessionToken).toBeCalledWith(token);
    });
  });
});
