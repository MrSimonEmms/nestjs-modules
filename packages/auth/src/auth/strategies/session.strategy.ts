/**
 * session.strategy
 */

/* Node modules */

/* Third-party modules */
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-http-bearer';

/* Files */
import UserService from '../../services/user.service';
import { IUser } from '../../interfaces/user.interface';

@Injectable()
export default class SessionStrategy extends PassportStrategy(Strategy, 'session') {
  @Inject(UserService)
  protected readonly userService: UserService;

  async validate(token: string): Promise<IUser | void> {
    const user = await this.userService.findBySessionToken(token);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
