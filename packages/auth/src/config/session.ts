/**
 * session
 */

/* Node modules */

/* Third-party modules */
import { registerAs } from '@nestjs/config';

/* Files */

export default registerAs('session', () => ({
  expiry: process.env.SESSION_EXPIRES ?? '30d',
}));
