/**
 * server
 */

/* Node modules */

/* Third-party modules */
import { registerAs } from '@nestjs/config';

/* Files */

export default registerAs('server', () => ({
  enableShutdownHooks: process.env.SERVER_ENABLE_SHUTDOWN_HOOKS === 'true',
  healthCheckTimeout: Number(process.env.SERVER_HEALTHCHECK_TIMEOUT ?? 1000),
  port: Number(process.env.SERVER_PORT ?? 3000),
}));
