/**
 * database
 */

/* Node modules */

/* Third-party modules */
import { registerAs } from '@nestjs/config';
import { LoggerOptions } from 'typeorm/logger/LoggerOptions';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

/* Files */

let logging: boolean | string | undefined = false;
const loggingVar = process.env.DB_LOGGING;

if (loggingVar === 'true') {
  logging = true;
} else if (loggingVar === 'false') {
  logging = false;
} else {
  logging = loggingVar;
}

/* SSL configuration should default to undefined */
let ssl;
if (process.env.DB_USE_SSL === 'true') {
  ssl = {
    ca: process.env.DB_SSL_CA,
    cert: process.env.DB_SSL_CERT,
    key: process.env.DB_SSL_KEY,
  };
}

export default registerAs(
  'db',
  (): TypeOrmModuleOptions => ({
    ssl,
    type: process.env.DB_TYPE as 'mysql' | 'mariadb',
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT ? Number(process.env.DB_PORT) : undefined,
    migrationsRun: process.env.DB_MIGRATIONS_RUN !== 'false',
    synchronize: process.env.DB_SYNC === 'true',
    logging: (logging as LoggerOptions) ?? true,
  }),
);
