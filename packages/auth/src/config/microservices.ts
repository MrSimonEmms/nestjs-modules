/**
 * microservices
 */

/* Node modules */

/* Third-party modules */
import { registerAs } from '@nestjs/config';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

/* Files */

export default registerAs('microservices', (): MicroserviceOptions[] => {
  const opts: MicroserviceOptions[] = []

  if (process.env.REDIS_URL) {
    opts.push({
      transport: Transport.REDIS,
      options: {
        url: process.env.REDIS_URL,
        retryAttempts: Number(process.env.REDIS_RETRY_ATTEMPTS ?? 0),
        retryDelay: Number(process.env.REDIS_RETRY_DELAY ?? 0),
      },
    })
  }

  return opts;
});
