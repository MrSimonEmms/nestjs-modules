/**
 * index
 *
 * Collates the individual config namespaces as a single
 * array which can be injected into the ConfigModule
 */

/* Node modules */

/* Third-party modules */

/* Files */
import database from './database';
import logger from './logger';
import microservices from './microservices';
import server from './server';
import session from './session';

export default [database, logger, microservices, server, session];
