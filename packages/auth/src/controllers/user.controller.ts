/**
 * user.controller
 */

/* Node modules */

/* Third-party modules */
import {
  BadRequestException,
  Body,
  Controller,
  HttpCode,
  HttpException,
  HttpStatus,
  Inject,
  Post,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  Crud,
  CrudAuth,
  CrudController,
  CrudRequest,
  CrudRequestInterceptor,
  Override,
  ParsedBody,
  ParsedRequest,
} from '@nestjsx/crud';
import { AuthGuard } from '@nestjs/passport';
import { validate } from 'class-validator';

/* Files */
import UserEntity from '../entities/user.entity';
import UserService from '../services/user.service';
import { IAuthInputDTO, IUserLoginDTO } from '../interfaces/user.interface';

@Crud({
  model: {
    type: UserEntity,
  },
  routes: {
    only: ['createOneBase', 'deleteOneBase', 'getOneBase', 'updateOneBase'],
  },
  params: {
    id: {
      primary: true,
      disabled: true,
    },
  },
  query: {
    exclude: ['password', 'tmpPassword'],
    persist: ['createdAt'],
  },
})
@CrudAuth({
  filter: (req: any) => {
    if (req.user) {
      return {
        id: req.user.id,
      };
    }
    return {};
  },
})
@Controller('user')
export default class UserController implements CrudController<UserEntity> {
  constructor(@Inject(UserService) public service: UserService) {}

  private static parseOutput(user: UserEntity): UserEntity {
    return {
      ...user,
      password: undefined,
    } as UserEntity;
  }

  get base(): CrudController<UserEntity> {
    return this;
  }

  @UseInterceptors(CrudRequestInterceptor)
  @HttpCode(200)
  @Post('/auth')
  async authenticate(@Body() { emailAddress, password }: IAuthInputDTO): Promise<IUserLoginDTO> {
    const user = await this.service.findByEmailAndPassword(emailAddress, password);

    if (!user) {
      throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
    }

    return {
      ...(await this.service.generateUserToken(user)),
      user: this.service.toDTO(user),
    };
  }

  @Override('createOneBase')
  async createUser(@ParsedRequest() crudReq: CrudRequest, @ParsedBody() dto: UserEntity) {
    return UserController.parseOutput(
      await this.base.createOneBase(crudReq, {
        ...dto,
        id: undefined, // Prevent ID being set
      } as UserEntity),
    );
  }

  @UseGuards(AuthGuard(['session']))
  @Override('getOneBase')
  async getUser(@ParsedRequest() req: CrudRequest) {
    return this.base.getOneBase(req);
  }

  @UseGuards(AuthGuard(['session']))
  @Override('updateOneBase')
  async updateOne(@Request() { user }, @ParsedRequest() req: CrudRequest, @Body() body) {
    const dto = new UserEntity();
    Object.keys(body).forEach((key) => {
      dto[key] = body[key];
    });
    /* Prevent any ID changes */
    dto.id = user.id;

    const errors = await validate(dto);
    if (errors.length > 0) {
      /* Failed validation */
      throw new BadRequestException(
        errors.map(({ constraints }) => Object.values(constraints)).flat(),
      );
    }

    return UserController.parseOutput(await this.base.updateOneBase(req, dto));
  }

  @UseGuards(AuthGuard(['session']))
  @Override('deleteOneBase')
  async deleteOne(@ParsedRequest() req: CrudRequest) {
    return this.base.deleteOneBase(req);
  }
}
