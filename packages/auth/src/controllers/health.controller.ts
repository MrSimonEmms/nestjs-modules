/**
 * health.controller
 */

/* Node modules */

/* Third-party modules */
import { Controller, Get, Inject } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Transport } from '@nestjs/microservices';
import {
  HealthCheck,
  HealthCheckService,
  HealthIndicatorFunction,
  MicroserviceHealthIndicator,
  TypeOrmHealthIndicator,
} from '@nestjs/terminus';

/* Files */

@Controller('health')
export default class HealthController {
  @Inject(ConfigService)
  private readonly config: ConfigService;

  @Inject(TypeOrmHealthIndicator)
  private readonly db: TypeOrmHealthIndicator;

  @Inject(HealthCheckService)
  private readonly health: HealthCheckService;

  @Inject(MicroserviceHealthIndicator)
  private readonly microservices: MicroserviceHealthIndicator;

  @Get()
  @HealthCheck()
  check() {
    const checks: HealthIndicatorFunction[] = [
      async () =>
        this.db.pingCheck('database', {
          timeout: this.config.get<number>('server.healthCheckTimeout'),
        }),
    ];

    checks.push(
      ...this.config.get('microservices').map((item) => async () => {
        const key = Transport[item.transport] ?? 'microservice';

        return this.microservices.pingCheck(key.toLowerCase(), item);
      }),
    );

    return this.health.check(checks);
  }
}
