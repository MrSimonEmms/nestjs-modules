import {MigrationInterface, QueryRunner} from "typeorm";

export class createUser1606767787012 implements MigrationInterface {
    name = 'createUser1606767787012'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `user` (`id` int NOT NULL AUTO_INCREMENT, `name` varchar(200) NOT NULL, `emailAddress` varchar(200) NOT NULL, `password` varchar(200) NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), UNIQUE INDEX `IDX_eea9ba2f6e1bb8cb89c4e672f6` (`emailAddress`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `session` (`id` int NOT NULL AUTO_INCREMENT, `token` varchar(200) NOT NULL, `expires` datetime NOT NULL, `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `userId` int NULL, INDEX `IDX_ccefef61d2b8556910d2d9ff1d` (`token`, `expires`), INDEX `IDX_af014c944177805ec21ef0bf20` (`expires`), UNIQUE INDEX `IDX_232f8e85d7633bd6ddfad42169` (`token`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `session` ADD CONSTRAINT `FK_3d2f174ef04fb312fdebd0ddc53` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("ALTER TABLE `session` DROP FOREIGN KEY `FK_3d2f174ef04fb312fdebd0ddc53`");
        await queryRunner.query("DROP INDEX `IDX_232f8e85d7633bd6ddfad42169` ON `session`");
        await queryRunner.query("DROP INDEX `IDX_af014c944177805ec21ef0bf20` ON `session`");
        await queryRunner.query("DROP INDEX `IDX_ccefef61d2b8556910d2d9ff1d` ON `session`");
        await queryRunner.query("DROP TABLE `session`");
        await queryRunner.query("DROP INDEX `IDX_eea9ba2f6e1bb8cb89c4e672f6` ON `user`");
        await queryRunner.query("DROP TABLE `user`");
    }

}
