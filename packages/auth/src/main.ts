/**
 * main
 */

/* Node modules */

/* Third-party modules */
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { Logger } from 'nestjs-pino';

/* Files */
import AppModule from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const config = app.get(ConfigService);
  const logger = app.get(Logger);

  logger.debug('Loading microservice configurations');
  await Promise.all(
    config.get('microservices').map((opts) => {
      const transport = Transport[opts.transport] ?? 'microservice';
      logger.log({ transport }, 'Configure Microservice');

      return app.connectMicroservice<MicroserviceOptions>(opts);
    }),
  );

  const port = config.get('server.port');

  if (config.get<boolean>('server.enableShutdownHooks', false)) {
    logger.log('Enabling shutdown hooks');
    app.enableShutdownHooks();
  }

  logger.debug('Starting microservices');
  await app.startAllMicroservicesAsync();

  logger.debug('Starting HTTP listener');
  await app.listen(port);
  logger.log('Application running', port);
}

bootstrap().catch((err) => {
  /* Unlikely to get to here but a final catchall */
  console.log(err.stack);
  process.exit(1);
});
