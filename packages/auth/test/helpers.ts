/**
 * helpers
 */

/* Node modules */

/* Third-party modules */
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as supertest from 'supertest';

/* Files */
import AppModule from '../src/app.module';
import { IUserLoginDTO } from '../src/interfaces/user.interface';

process.env.SOURCE_DIR = 'auth';

const dataIngester = require('../../../data/init');

export { supertest };

let app: INestApplication;

export const createApp = async () => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  app = moduleFixture.createNestApplication();
  await app.init();

  return app;
};

export const destroyApp = async () => {
  if (app) {
    await app.close();

    /* Remove definition to prevent being run again */
    app = undefined;
  }
};

export const request = (): supertest.SuperTest<supertest.Test> => {
  if (!app) {
    throw new Error('App must be created with createApp function');
  }

  return supertest(app.getHttpServer());
};

export const login = async (
  emailAddress: string = 'test@test.com',
  password: string = 'q1w2e3r4',
): Promise<IUserLoginDTO> => {
  if (!app) {
    await createApp();
  }

  const { body } = await request()
    .post('/user/auth')
    .send({
      emailAddress,
      password,
    })
    .expect(200);

  return body;
};

/* Add the stubbed data into the database */
beforeEach(() => dataIngester(false));

/* Close the connection */
afterEach(() => destroyApp());
