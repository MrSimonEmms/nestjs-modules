/**
 * auth.e2e-spec
 */

/* Node modules */

/* Third-party modules */

/* Files */
import { createApp, login, request } from '../helpers';

describe('/user', () => {
  let app: any;

  beforeEach(() => createApp());

  describe('DELETE', () => {
    describe('/', () => {
      beforeEach(() => {
        app = request().delete('/user');
      });

      it('should return 401 if not logged in', () => app.expect(401));

      it('should delete the data if logged in', async () => {
        const { token } = await login();

        await app.set('authorization', `bearer ${token}`).expect(200, {});
      });
    });
  });

  describe('GET', () => {
    describe('/', () => {
      beforeEach(() => {
        app = request().get('/user');
      });

      it('should return 401 if not logged in', () => app.expect(401));

      it('should return the user data if logged in', async () => {
        const { token, user } = await login();

        const { body } = await app.set('authorization', `bearer ${token}`).expect(200);

        expect(body).toEqual(user);
      });
    });
  });

  describe('PATCH', () => {
    describe('/', () => {
      beforeEach(() => {
        app = request().patch('/user');
      });

      it('should return 401 if not logged in', () => app.expect(401));

      it('should error if password already in use', async () => {
        const { token } = await login();

        await app
          .set('authorization', `bearer ${token}`)
          .send({
            emailAddress: 'test2@test2.com',
          })
          .expect(400);
      });

      it('should update the name', async () => {
        const { token, user } = await login();

        const name = 'new name';

        const { body } = await app
          .set('authorization', `bearer ${token}`)
          .send({
            name,
          })
          .expect(200);

        expect(body).toEqual({
          ...user,
          name,
          updatedAt: expect.any(String),
        });
        expect(body.updatedAt).not.toEqual(user.updatedAt);
      });

      it('should update the email', async () => {
        const { token, user } = await login();

        const emailAddress = 'test@2test.com';

        const { body } = await app
          .set('authorization', `bearer ${token}`)
          .send({
            emailAddress,
          })
          .expect(200);

        expect(body).toEqual({
          ...user,
          emailAddress,
          updatedAt: expect.any(String),
        });
        expect(body.updatedAt).not.toEqual(user.updatedAt);
      });

      it('should update the password', async () => {
        const { token, user } = await login();

        const password = 'new-password';

        const { body } = await app
          .set('authorization', `bearer ${token}`)
          .send({
            password,
          })
          .expect(200);

        expect(body).toEqual({
          ...user,
          updatedAt: expect.any(String),
        });
        expect(body.updatedAt).not.toEqual(user.updatedAt);

        await login(user.emailAddress, password);
      });

      it('should update everything', async () => {
        const { token, user } = await login();

        const data = {
          name: 'hello',
          emailAddress: 'test9@test.com',
          password: 'some-new-password',
        };

        const { body } = await app.set('authorization', `bearer ${token}`).send(data).expect(200);

        expect(body).toEqual({
          ...user,
          ...data,
          password: undefined,
          updatedAt: expect.any(String),
        });
        expect(body.updatedAt).not.toEqual(user.updatedAt);

        await login(data.emailAddress, data.password);
      });
    });
  });

  describe('POST', () => {
    describe('/', () => {
      beforeEach(() => {
        app = request().post('/user');
      });

      it('should error if password already in use', async () => {
        await app
          .send({
            emailAddress: 'test2@test2.com',
          })
          .expect(400);
      });

      it('should create a user', async () => {
        const data = {
          emailAddress: 'test9@test.com',
          password: 'my-new-password',
          name: 'user name',
        };

        const { body } = await app.send(data).expect(201);

        expect(body).toEqual({
          id: expect.any(Number),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
          emailAddress: data.emailAddress,
          name: data.name,
        });

        await login(data.emailAddress, data.password);
      });
    });
  });
});
