/**
 * auth.e2e-spec
 */

/* Node modules */

/* Third-party modules */
import * as uuid from 'uuid';

/* Files */
import { createApp, request } from '../helpers';

describe('/user/auth', () => {
  let app: any;

  beforeEach(() => createApp());

  describe('POST', () => {
    describe('/', () => {
      beforeEach(() => {
        app = request().post('/user/auth');
      });

      it('should validate a user', async () => {
        const emailAddress = 'test@test.com';

        const { body } = await app
          .send({
            emailAddress,
            password: 'q1w2e3r4',
          })
          .expect(200);

        expect(body).toEqual(
          expect.objectContaining({
            expires: expect.any(String),
            token: expect.any(String),
            user: {
              emailAddress,
              id: expect.any(Number),
              name: 'Test Testington',
              createdAt: expect.any(String),
              updatedAt: expect.any(String),
            },
          }),
        );

        expect(uuid.validate(body.token)).toBe(true);
        expect(new Date(body.user.createdAt).getTime()).toBeLessThanOrEqual(Date.now());
        expect(new Date(body.user.updatedAt).getTime()).toBeLessThanOrEqual(Date.now());
      });

      it('should not validate a user with valid email but invalid password', () =>
        app
          .send({
            emailAddress: 'test@test.com',
            password: 'some-invalid-password',
          })
          .expect(401));

      it('should not validate a user with invalid email but valid password', () =>
        app
          .send({
            emailAddress: 'notarealuser@fail.com',
            password: 'q1w2e3r4',
          })
          .expect(401));

      it('should not validate a user with invalid email and invalid password', () =>
        app
          .send({
            emailAddress: 'notarealuser@fail.com',
            password: 'some-invalid-password',
          })
          .expect(401));

      it('should not validate a user with empty credentials', () =>
        app
          .send({
            emailAddress: '',
            password: '',
          })
          .expect(401));

      it('should not validate a user with empty body', () => app.send().expect(401));

      it('should not validate a user with no body', () => app.expect(401));
    });
  });
});
