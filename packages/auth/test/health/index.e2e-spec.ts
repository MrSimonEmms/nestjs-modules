/**
 * index.e2e-spec
 */

/* Node modules */

/* Third-party modules */

/* Files */
import { createApp, request } from '../helpers';

describe('/health', () => {
  let app: any;

  beforeEach(() => createApp());

  describe('GET', () => {
    describe('/', () => {
      beforeEach(() => {
        app = request().get('/health');
      });

      it('should return a valid health object', () => app.expect(200));
    });
  });
});
