/**
 * typeorm
 *
 * This is only meant as a helper for development. It converts
 * the config into a typeorm friendly config and executes the
 * command provided
 */

/* Node modules */
const { exec } = require('child_process');
const { promises: fs } = require('fs');
const os = require('os');
const path = require('path');

/* Third-party modules */

/* Files */

const cmd = process.argv.slice(2);
const baseDir = process.env.BASE_DIR || process.cwd();
const srcDir = path.join(baseDir, 'src');
const configDir = os.tmpdir();

async function main() {
  const connection = {
    name: 'default',
    type: 'mysql',
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT ? Number(process.env.DB_PORT) : undefined,
    entities: [path.join(srcDir, '**', '*.entity{.ts,.js}')],
    migrations: [path.join(srcDir, 'migrations', '*{.ts,.js}')],
  };

  /* Write the config to a file */
  const configFile = path.join(configDir, 'connection.json');

  await fs.writeFile(configFile, JSON.stringify(connection, null, 2), 'utf8');

  const tsNode = path.join(srcDir, '..', 'node_modules', '.bin', 'ts-node');
  const target = path.join(srcDir, '..', 'node_modules', '.bin', 'typeorm');

  exec(
    `${tsNode} ${target} ${cmd.join(' ')} --config="${path.relative(
      srcDir,
      configDir,
    )}/connection.json"`,
    {},
    (err, stdout, stderr) => {
      if (err) {
        throw err;
      }
      if (stderr) {
        // eslint-disable-next-line @typescript-eslint/no-throw-literal
        throw stderr;
      }

      console.log(stdout);
    },
  );
}

main().catch((err) => {
  console.log(err.stack);
  process.exit(1);
});
